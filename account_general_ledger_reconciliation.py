# This file is part of Adiczion's Tryton Module.
# The COPYRIGHT and LICENSE files at the top level of this repository
# contains the full copyright notices and license terms.
import logging
from trytond.model import fields
from trytond.pool import PoolMeta

logger = logging.getLogger(__name__)


class GeneralLedgerLine(metaclass=PoolMeta):
    'General Ledger Line'
    __name__ = 'account.general_ledger.line'
    reconciliation = fields.Many2One('account.move.reconciliation',
        'Reconciliation')
